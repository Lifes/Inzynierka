#pragma once
#include "glm.hpp"
#include <vector>

class Polygon;

struct Plane
{
	//plane representation by eq (p - p0) * n = 0
	//where p is any point on plane
	//p0 is given point on plane
	//n is plane normal vector
	Plane();
	Plane(const Polygon& polygon);
	Plane(const std::vector<glm::vec3>& vertices);
	Plane(const std::vector<float>& vertices);
	~Plane();

	glm::vec3 n;
	glm::vec3 p0;

	//d is stored for other plane representation
	//p * n = d
	float d = 0;
};

//find intersection between plane and line defined by two given points and puts it in result
//returns 0 if no intersection
//1 if intersection found
//2 if intersection found and contained in segment between p0 and p1
//-1 if line is on the plane
//http://geomalgorithms.com/a05-_intersect-1.html
int FindPlaneLineIntersection(const Plane& plane, const glm::vec3& line_p0, const glm::vec3& line_p1, glm::vec3* result);