#pragma once
#include <vector>
#include "Object.h"
#include <memory>
#include "Tree.h"
#include "PVSTree.h"

class Scene
{
public:
	~Scene();
	

	std::vector<Object*>& GetSceneObjects() { return m_Objects; }
	const std::vector<std::shared_ptr<Polygon>>& GetAllScenePolygons() { return m_Polygons; }
	const std::shared_ptr<Tree> GetSceneTree() { return m_SceneTree; }
	const std::shared_ptr<PVSTree> GetPvsSceneTree() const { return m_PvsSceneTree; }

	void AddObject(const std::string& pathToPositions, const std::string& pathToColors);
	void AddObject(const Object& object);
	void UpdatePolygonList();

private:
	std::vector<Object*> m_Objects;
	std::vector<std::shared_ptr<Polygon>> m_Polygons;
	std::shared_ptr<Tree> m_SceneTree;
	std::shared_ptr<PVSTree> m_PvsSceneTree;

	bool m_PolygonsDirty = true;
};