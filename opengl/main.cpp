#include <SDL.h>

#include <iostream>
#include "Libs/glew/glew.h"
#include "Time.h"
#include "Render.h"
#include "Color.h"
#include "Scene.h"
#include "Object.h"
#include "Input.h"
#include "SetupPresentation.h"
#include <queue>

constexpr int RES_WIDTH = 1280;
constexpr int RES_HEIGHT = 720;

SDL_Window *mainWindow;
SDL_GLContext mainContext;

bool Init();
void Cleanup();


int main(int argc, char ** argv)
{

	if(!Init())
		return -1;

	Time time;

	Color bg_color = Color(0.3f, 0.3f, 0.3f);
	Camera camera;
	Renderer renderer(bg_color, mainWindow, &camera, "../vert1.glsl", "../frag1.glsl");

	Scene* scenes[2];
	int active_scene = 0;
	scenes[0] = SetupBasicScene();
	//scenes[1] = SetupSimpleLargeScene();
	scenes[1] = SetupConvexScene(5);

	bool isQuitDemanded = false;

	//main program loop
	float update_time = 0.5f;
	std::queue<float> dt_queue;
	while (!isQuitDemanded)
	{
		auto dt = time.Update();
		isQuitDemanded = Input::Instance().ProcessInput();
		camera.Update(dt);

		renderer.RenderFrame(scenes[Input::Instance().active_scene]);

		//get frame dt
		dt_queue.push(dt * 1000);
		if (dt_queue.size() > 30)
		{
			dt_queue.pop();
		}
		update_time -= dt;
		if (update_time < 0.f)
		{
			float avg = 0.f;
			int i = 0;
			while(!dt_queue.empty())
			{
				avg += dt_queue.front();
				dt_queue.pop();
				++i;
			}
			avg /= i;
			std::string title = "dt = " + std::to_string(avg) + ", polygons = " + std::to_string(renderer.GetPolyCount());
			SDL_SetWindowTitle(mainWindow, title.c_str());
			update_time = 0.3f;
		}

	}
	for (auto scene : scenes)
	{
		delete scene;
	}
	Cleanup();

	return 0;
}



bool Init()
{

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		std::cerr << "Failed to init SDL\n";
		return false;
	}

	mainWindow = SDL_CreateWindow(
		"My Game",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		RES_WIDTH,
		RES_HEIGHT,
		SDL_WINDOW_OPENGL
	);

	if (!mainWindow)
	{
		std::cerr << "Failed to create a window\n";
		return false;
	}

	mainContext = SDL_GL_CreateContext(mainWindow);

	glewExperimental = GL_TRUE;
	glewInit();

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	SDL_GL_SetSwapInterval(0);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthFunc(GL_LEQUAL);

	return true;
}

void Cleanup()
{
	SDL_GL_DeleteContext(mainContext);
	SDL_DestroyWindow(mainWindow);
	SDL_Quit();
}
