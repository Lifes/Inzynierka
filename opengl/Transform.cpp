#include "Transform.h"
#include "gtc\matrix_transform.hpp"
#include "gtx\quaternion.hpp"

Transform::Transform()
= default;

Transform::~Transform()
= default;

void Transform::SetRotation(const glm::quat& rotation)
{
	m_Rotation = rotation;
	m_IsDirty = true;
}

void Transform::Rotate(const glm::quat& rotation)
{
	m_Rotation *= rotation;
	glm::normalize(m_Rotation);
	m_IsDirty = true;
}

void Transform::SetPosition(const glm::vec3& position)
{
	m_Position = position;
	m_IsDirty = true;
}

void Transform::Translate(const glm::vec3& translation)
{
	m_Position += translation;
	m_IsDirty = true;
}

void Transform::SetScale(const glm::vec3& scale)
{
	m_Scale = scale;
	m_IsDirty = true;
}

void Transform::UpdateTransformMatrix() const
{
	if (!m_IsDirty)
		return;

	glm::mat4 position = glm::translate(glm::mat4(1), m_Position);
	glm::mat4 rotation = glm::toMat4(m_Rotation);
	glm::mat4 scale = glm::scale(glm::mat4(1), m_Scale);
	SetMatrix(position, rotation, scale);
	m_IsDirty = false;
}

const glm::mat4& Transform::GetTransformMatrix() const
{
	UpdateTransformMatrix();
	return m_Transform;
}

glm::vec3 Transform::GetAxisZ() const
{
	UpdateTransformMatrix();
	return glm::vec3(m_Transform[0][2], m_Transform[1][2], m_Transform[2][2]);
}

glm::vec3 Transform::GetAxisY() const
{
	UpdateTransformMatrix();
	return glm::vec3(m_Transform[0][1], m_Transform[1][1], m_Transform[2][1]);
}

glm::vec3 Transform::GetAxisX() const
{
	UpdateTransformMatrix();
	return glm::vec3(m_Transform[0][0], m_Transform[1][0], m_Transform[2][0]);
}

void Transform::SetMatrix(const glm::mat4& position, const glm::mat4& rotation, const glm::mat4& scale) const
{
	m_Transform = position * rotation * scale;
}

void CameraTransform::SetMatrix(const glm::mat4& position, const glm::mat4& rotation, const glm::mat4& scale) const
{
	m_Transform = scale * rotation * position;
}
