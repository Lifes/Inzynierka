#include "Input.h"
#include "Render.h"
#include "Libs/glew/glew.h"
#include "SDL.h"
#include "SetupPresentation.h"

bool Input::ProcessInput()
{
	bool isQuitDemanded = false;
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			isQuitDemanded = true;

		if (event.type == SDL_KEYDOWN)
		{
			switch (event.key.keysym.sym)
			{
			case SDLK_f:
				Renderer::m_RenderWireframe = !Renderer::m_RenderWireframe;
				break;
			case SDLK_b:
				Renderer::m_EnableBackfaceCulling = !Renderer::m_EnableBackfaceCulling;
				break;
			case SDLK_z:
				if (!Renderer::m_EnableDepthBuffer)
				{
					Renderer::m_EnableDepthBuffer = true;
					glEnable(GL_DEPTH_TEST);
				}
				else
				{
					Renderer::m_EnableDepthBuffer = false;
					glDisable(GL_DEPTH_TEST);
				}
				break;
			case SDLK_F1:
				active_scene = 0;
				break;
			case SDLK_F2:
				active_scene = 1;
				break;
			case SDLK_ESCAPE:
				isQuitDemanded = true;
				break;
			default:
				break;
			}
		}
	}
	m_PreviousMouseX = m_CurrentMouseX;
	m_PreviousMouseY = m_CurrentMouseY;

	SDL_GetMouseState(&m_CurrentMouseX, &m_CurrentMouseY);

	return isQuitDemanded;
}

void Input::GetMouseRelativeState(int* x, int* y) const
{
	*x = m_CurrentMouseX - m_PreviousMouseX;
	*y = m_CurrentMouseY - m_PreviousMouseY;
}
