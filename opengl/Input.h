#pragma once


class Input
{
	friend int main(int argc, char ** argv);
public:
	static Input& Instance()
	{
		static Input* instance = new Input();
		return *instance;
	}

	~Input() = default;;

	void GetMouseRelativeState(int* x, int* y) const;
	int active_scene;
private:
	bool ProcessInput();
	Input() = default;;


	int m_CurrentMouseX;
	int m_CurrentMouseY;
	int m_PreviousMouseX;
	int m_PreviousMouseY;
};