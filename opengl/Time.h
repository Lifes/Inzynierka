#pragma once
#include "SDL.h"

class Time
{
public:
	Time();
	~Time();

	float Update();
	float DeltaTime() { return m_DeltaTime; }

private:
	unsigned int m_LastFrameTime;
	unsigned int m_ThisFrameTime;
	float m_DeltaTime;
};

