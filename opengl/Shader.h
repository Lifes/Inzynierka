#pragma once
#include <string>
#include "glm.hpp"

class Shader
{
public:
	Shader(const std::string& vertexShaderPath, const std::string& fragmentShaderPath = "");
	~Shader();

	void Run();
	void ReloadVertexShader(const std::string& vertexShaderPath);
	void ReloadFragmentShader(const std::string& fragmentShaderPath);

	void SetMVPMatrix(const glm::mat4& mvp);

private:

	std::string ReadFile(const std::string& path) const;
	bool LoadVertexShader();
	bool LoadFragmentShader();
	bool LinkShaders();

	void PrintDebugMessage(int errorCode) const;

	const unsigned int POSITION_ATT_LOCATION = 0;
	const unsigned int COLOR_ATT_LOCATION = 1;

	unsigned int m_Program;
	unsigned int m_VertexShader;
	unsigned int m_FragmentShader;

	std::string m_VertexShaderPath;
	std::string m_FragmentShaderPath;
};