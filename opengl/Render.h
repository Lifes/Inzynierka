#pragma once
#include <string>
#include "SDL.h"
#include "Color.h"
#include "glm.hpp"
#include "Camera.h"
#include "Tree.h"
#include <memory>

class Scene;
class Shader;
class Polygon;
class PVSTree;



class Renderer
{
	friend class Input;
public:
	Renderer(const Color& backgroundColor, SDL_Window* mainWindow, Camera* camera, const std::string& vertexShaderPath, const std::string& fragmentShaderPath = "");
	~Renderer();

	void StartFrame();
	void EndFrame();
	void RenderScene(Scene* scene);

	void RenderFrame(Scene* scene);

	void RenderPolygon(std::shared_ptr<Polygon> polygon);
	void RenderTree(std::shared_ptr<Tree> tree);
	void RenderTree(std::shared_ptr<PVSTree> tree);

	void SetCamera(Camera* camera) { m_Camera = camera; }
	void SetBackgroudColor(const Color& bgColor) { m_BackgroundColor = bgColor; }

	uint32_t GetPolyCount() { return m_PolyCount; }

private:
	void RenderTreeNode(Node* node, const glm::vec3& viewerPos);
	void RenderPVSTree(std::shared_ptr<PVSTree> tree, uint32_t leafId);
	bool IsPVSLeafInCameraFrustum(std::shared_ptr<PVSTree> tree, uint32_t leafId);

	Camera* m_Camera;
	Shader* m_Shader;
	SDL_Window* m_MainWindow;
	Color m_BackgroundColor;

	uint32_t m_PolyCount = 0;

	static bool m_RenderWireframe;
	static bool m_EnableBackfaceCulling;
	static bool m_EnableDepthBuffer;
};

void ClearColor(const Color& color, SDL_Window* mainWindow);