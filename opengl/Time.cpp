#include "Time.h"

Time::Time()
	: m_LastFrameTime(0)
	, m_ThisFrameTime(0)
	, m_DeltaTime(0)
{
}

Time::~Time()
= default;

float Time::Update()
{
	m_LastFrameTime = m_ThisFrameTime;
	m_ThisFrameTime = SDL_GetTicks();
	m_DeltaTime = static_cast<float>(m_ThisFrameTime - m_LastFrameTime);
	m_DeltaTime /= 1000.f; //convert from ms to s
	return m_DeltaTime;
	//return 0.016f;
}
