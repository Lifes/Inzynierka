#include "PVSTree.h"
#include "Object.h"
#include "Float.h"

#include "glm.hpp"
#include "gtc\matrix_transform.hpp"

#include <cmath>
#include <queue>
#include <tuple>
#include <array>
#include <limits>

PVSTree::PVSTree(polygon_list polygons)
{
	m_Nodes.emplace_back();
	MakeTree(0, polygons);
	BuildPortals();
	CalculatePVS();
}

void PVSTree::MakeTree(uint32_t nodeId, polygon_list& polygons)
{
	polygon_list front_polygons;
	polygon_list back_polygons;
	std::queue<poly_tuple> polygons_to_replace;

	m_Nodes[nodeId].plane = GetBestSplitter(polygons, front_polygons);
	
	for (const auto& poly : polygons)
	{
		auto transform = poly->GetParent()->GetTransform();
		const auto& vertices = poly->m_Vertices;
		std::vector<glm::vec3> front;
		std::vector<glm::vec3> back;

		PositionVerticesToPlane(vertices, transform, m_Planes[m_Nodes[nodeId].plane], front, back);
		PositionPolygonToPlane(front, back, m_Planes[m_Nodes[nodeId].plane], poly, back_polygons, front_polygons, polygons_to_replace);
	}

	AddQueuedPolygonsToObjects(polygons_to_replace);
	
	BBox bbox;
	CalculateBBox(&bbox, front_polygons);
	BBox leaf_box = bbox;
	CalculateBBox(&bbox, back_polygons);
	m_Nodes[nodeId].bounding_box = bbox;

	bool is_front_leaf = true;
	for (const auto& poly : front_polygons)
	{
		if (!poly->IsSplitter())
		{
			is_front_leaf = false;
			break;
		}
	}

	if (is_front_leaf)
	{
		m_Nodes[nodeId].is_leaf = true;
		Leaf leaf;
		leaf.start_polygon = m_Polygons.size();
		for (const auto& poly : front_polygons)
		{
			m_Polygons.push_back(poly);
		}
		leaf.end_polygon = m_Polygons.size();
		leaf.bounding_box = leaf_box;
		m_Nodes[nodeId].front = m_Leafs.size();
		m_Leafs.push_back(leaf);	
	}
	else
	{
		m_Nodes.emplace_back();
		m_Nodes[nodeId].front = m_Nodes.size() - 1;
		MakeTree(m_Nodes.size() - 1, front_polygons);
	}

	if (back_polygons.empty())
	{
		m_Nodes[nodeId].back = -1;
	}
	else
	{
		m_Nodes.emplace_back();
		m_Nodes[nodeId].back = m_Nodes.size() - 1;
		MakeTree(m_Nodes.size() - 1, back_polygons);
	}
}


void PVSTree::BuildPortals()
{
	for (uint32_t i = 0; i < m_Nodes.size(); ++i)
	{
		auto portal = CalculatePortal(i);
		auto portals = ClipPortal(portal, 0);

		//add only real portals, discard fake
		for (const auto& portal : portals)
		{
			int32_t j = -1;
			if (portal == nullptr || portal->leafs_amount != 2) //portal is fake
			{
				continue;
			}
			else if (IsPortalDuplicate(portal, &j)) //portal is fine but other one for same leafs exists
			{
				if (j == -1) //portal is the worse duplicate
				{
					continue;
				}
				else //portal is the better duplicate
				{
					m_Portals[j] = portal; //switch the portal for the better one
					continue;
				}
			}
			else //portal is fine
			{
				//add portal to confirmed portals array
				m_Portals.push_back(portal);
				//add this portal index to its leafs
				m_Leafs[portal->leafs[0]].portals_indices.push_back(m_Portals.size() - 1);
				m_Leafs[portal->leafs[1]].portals_indices.push_back(m_Portals.size() - 1);
			}
		}
	}

	for (uint32_t i = 0; i < m_Portals.size(); ++i)
	{
		m_Portals[i]->id = i;
	}
}


void PVSTree::CalculatePVS()
{
	for (uint32_t leaf_id = 0; leaf_id < m_Leafs.size(); ++leaf_id)
	{
		Leaf& source_leaf = m_Leafs[leaf_id];
		std::vector<uint32_t> leaf_pvs;
		leaf_pvs.push_back(leaf_id);
		for (uint32_t source_portal_index : source_leaf.portals_indices)
		{
			auto source_portal = m_Portals[source_portal_index];
			uint32_t destination_index = source_portal->leafs[0];
			if (destination_index == leaf_id)
			{
				destination_index = source_portal->leafs[1];
			}
			
			bool dest_in_pvs = false;
			for (auto p : leaf_pvs)
			{
				if (destination_index == p)
				{
					dest_in_pvs = true;
					break;
				}	
			}
			if(!dest_in_pvs)
				leaf_pvs.push_back(destination_index);

			Leaf& dest_leaf = m_Leafs[destination_index];
			for (uint32_t dest_portal_index : dest_leaf.portals_indices)
			{
				auto dest_portal = m_Portals[dest_portal_index];
				if (dest_portal_index != source_portal_index) //if source portal is not the same one as destination
				{
					//check if not on same plane
					int8_t position = PositionVerticesToPlane(dest_portal->portal.m_Vertices, Plane(source_portal->portal.m_Vertices));
					if (position != 0) //not on same plane
					{
						//recursively check other leafs
						std::vector<uint32_t> visited; //list of visited portals, to avoid infinite loops
						CalculateLeafsPVS(leaf_id, source_portal, dest_portal, destination_index, leaf_pvs, visited);
					}
				}
			}
		}
		m_PvsData.push_back(leaf_pvs);
	}
}

void PVSTree::CalculateLeafsPVS(uint32_t leafId, std::shared_ptr<Portal> sourcePortal, std::shared_ptr<Portal> destPortal, uint32_t destLeafId, std::vector<uint32_t>& pvs, std::vector<uint32_t>& visited)
{
	//check if we didn't already check this generator portal from this destination portal
	for (auto v : visited)
	{
		if (v == destPortal->id)
			return;
	}
	visited.emplace_back(destPortal->id);

	uint32_t generator_leaf_id = destPortal->leafs[0];
	if (generator_leaf_id == destLeafId)
		generator_leaf_id = destPortal->leafs[1];

	bool is_already_in_pvs = false;
	for (auto p : pvs)
	{
		if (generator_leaf_id == p)
			is_already_in_pvs = true;
	}
	if(!is_already_in_pvs)
		pvs.push_back(generator_leaf_id);

	glm::vec3 source_center = (m_Leafs[leafId].bounding_box.min + m_Leafs[leafId].bounding_box.max) / 2.f;
	glm::vec3 destination_center = (m_Leafs[destLeafId].bounding_box.min + m_Leafs[destLeafId].bounding_box.max) / 2.f;
	
	//find positioning of source leaf to source portal

	Plane source_plane(sourcePortal->portal.m_Vertices);
	Plane dest_plane = Plane(destPortal->portal.m_Vertices);
	int8_t source_pos = PositionVertexToPlane(source_center, source_plane);
	int8_t dest_pos = PositionVertexToPlane(destination_center, dest_plane);

	for (uint32_t i = 0; i <m_Leafs[generator_leaf_id].portals_indices.size(); ++i)
	{
		glm::uint32_t generator_portal_index = m_Leafs[generator_leaf_id].portals_indices[i];
		auto& generatorPortal = m_Portals[generator_portal_index];
		if(generatorPortal == destPortal)
			continue;

		std::shared_ptr<Portal> source_portal = std::make_shared<Portal>(*sourcePortal);
		std::shared_ptr<Portal> generator_portal = std::make_shared<Portal>(*generatorPortal);

		int8_t generator_pos = PositionVerticesToPlane(generatorPortal->portal.m_Vertices, source_plane);
		if (source_pos == generator_pos || generator_pos == 0)
			continue;
	
		generator_pos = PositionVerticesToPlane(generatorPortal->portal.m_Vertices, dest_plane);
		if (dest_pos == generator_pos || generator_pos == 0)
			continue;

		bool is_generator_valid = ClipWithAntiPenumbra(source_portal, destPortal, generator_portal);

		if (!is_generator_valid)
			continue;

		//clip it the other way around as well for better precision
		bool is_source_valid = ClipWithAntiPenumbra(generator_portal, destPortal, source_portal);

		if(!is_source_valid)
			continue;

		CalculateLeafsPVS(leafId, sourcePortal, generator_portal, generator_leaf_id, pvs, visited);
	}
}

bool PVSTree::ClipWithAntiPenumbra(std::shared_ptr<Portal> source, std::shared_ptr<Portal> destination, std::shared_ptr<Portal> generator)
{
	std::shared_ptr<Portal> tsource = source;
	std::shared_ptr<Portal> tdest = destination;

	std::vector<Plane> clip_planes;

	//build the anti-penumbra
	for (uint8_t i = 0; i < 2; ++i)
	{
		if (i == 1)
		{
			tsource = destination;
			tdest = source;
		}

		for  ( uint32_t i = 0; i + 2 < tsource->portal.m_Vertices.size();  i += 3)
		{
			glm::vec3 source_vertex(tsource->portal.m_Vertices[i], tsource->portal.m_Vertices[i + 1], tsource->portal.m_Vertices[i + 2]);
			uint8_t vertex_pos = PositionVertexToPlane(source_vertex, Plane(tdest->portal.m_Vertices));
			if (vertex_pos == 0) // if vertex is on plane, we cant build new plane from it
				continue;

			auto dest_size = tdest->portal.m_Vertices.size();
			glm::vec3 current_dest_vertex(tdest->portal.m_Vertices[dest_size- 3], tdest->portal.m_Vertices[dest_size - 2], tdest->portal.m_Vertices[dest_size - 1]);
			for (uint32_t j = 0; j + 2 < dest_size; j += 3)
			{
				glm::vec3 next_dest_vertex(tdest->portal.m_Vertices[j], tdest->portal.m_Vertices[j + 1], tdest->portal.m_Vertices[j + 2]);
				std::vector<glm::vec3> vertices{ source_vertex, current_dest_vertex, next_dest_vertex };
				Plane plane(vertices);

				glm::int8_t source_pos = PositionVerticesToPlane(tsource->portal.m_Vertices, plane);
				glm::int8_t dest_pos = PositionVerticesToPlane(tdest->portal.m_Vertices, plane);
				if ((source_pos == 1 && dest_pos == -1)
					|| (source_pos == -1 && dest_pos == 1)) //portals on opposite sides of plane
				{
					clip_planes.push_back(plane);
				}
			}
		}
	}

	for (const auto& clip_plane : clip_planes)
	{
		int8_t gen_portal_positioning = PositionVerticesToPlane(generator->portal.m_Vertices, clip_plane);
		int8_t src_portal_positioning = PositionVerticesToPlane(source->portal.m_Vertices, clip_plane);

		if (src_portal_positioning == gen_portal_positioning || gen_portal_positioning == 0)
		{//portals are on the same side of plane
			generator = nullptr;
			return false;
		}
		if ((src_portal_positioning == 1 && gen_portal_positioning == -1) 
			|| (src_portal_positioning == -1 && gen_portal_positioning == 1)) 
		{ //portals are on the opposite sides of plane
			continue;
		}

		if (gen_portal_positioning == 2) //generator portal is crossing the plane
		{
			std::array<std::shared_ptr<Polygon>, 2> results;
			results[0] = std::make_shared<Polygon>();
			results[1] = std::make_shared<Polygon>();
			SplitPolygon(std::make_shared<Polygon>(generator->portal), results, clip_plane);
			int32_t pos = PositionVerticesToPlane(results[0]->m_Vertices, clip_plane);
			if (src_portal_positioning == 1) //new portal is back portal
			{
				if (pos == 1) //results[0] is front
					generator->portal = *results[1];
				else
					generator->portal = *results[0];
			}
			else //new portal is front portal
			{
				if (pos == 1) //results[0] is front
					generator->portal = *results[0];
				else
					generator->portal = *results[1];
			}
		}
	}
	return true;
}

void PVSTree::CalculateBBox(BBox* box, polygon_list& polygons)
{
	for (const auto& poly : polygons)
	{
		CalculateBBox(box, poly);
	}
}

void PVSTree::CalculateBBox(BBox* box, std::shared_ptr<Polygon> polygon)
{
	std::vector<float> vertices = polygon->m_Vertices;
	glm::mat4 transform(1);
	Object* parent = polygon->GetParent();
	if (parent != nullptr)
		transform = parent->GetTransform();
	for (uint32_t i = 0; i + 3 < vertices.size(); i += 3)
	{

		glm::vec3 ws_p = static_cast<glm::vec3>(transform * glm::vec4(vertices[i], vertices[i + 1], vertices[i + 2], 1.f));
		if (ws_p.x < box->min.x)
			box->min.x = ws_p.x;
		if (ws_p.y < box->min.y)
			box->min.y = ws_p.y;
		if (ws_p.z < box->min.z)
			box->min.z = ws_p.z;

		if (ws_p.x > box->max.x)
			box->max.x = ws_p.x;
		if (ws_p.y > box->max.y)
			box->max.y = ws_p.y;
		if (ws_p.z > box->max.z)
			box->max.z = ws_p.z;
	}
}

std::vector<std::shared_ptr<Portal>> PVSTree::ClipPortal(std::shared_ptr<Portal> portal, uint32_t node_id)
{
	if (portal == nullptr)
		return std::vector<std::shared_ptr<Portal>>();

	std::vector<glm::vec3> front;
	std::vector<glm::vec3> back;

	PositionVerticesToPlane(portal->portal.m_Vertices, glm::mat4(1), m_Planes[m_Nodes[node_id].plane], front, back);
	if (front.empty() && back.empty()) //on plane
	{
		return ClipOnPlanePortal(node_id, portal);
	}
	else if (back.empty())
	{
		return ClipInFrontPortal(node_id, portal);
	}
	else if (front.empty())
	{
		return ClipOnBackPortal(node_id, portal);
	}
	else
	{
		return ClipSpanningPortal(portal, node_id);
	}

}

std::vector<std::shared_ptr<Portal>> PVSTree::ClipSpanningPortal(std::shared_ptr<Portal> portal, uint32_t node_id)
{
	std::vector<std::shared_ptr<Portal>> front_portals, back_portals;

	std::array<std::shared_ptr<Polygon>, 2> polygons;
	std::shared_ptr<Portal> front_portal = std::make_shared<Portal>();
	std::shared_ptr<Portal> back_portal = std::make_shared<Portal>();
	front_portal->leafs_amount = portal->leafs_amount;
	back_portal->leafs_amount = portal->leafs_amount;
	front_portal->leafs = portal->leafs;
	back_portal->leafs = portal->leafs;

	//to fit to existing splitting function interface
	polygons[0] = std::make_shared<Polygon>();
	polygons[1] = std::make_shared<Polygon>();
	auto portal_poly = std::make_shared<Polygon>(portal->portal);

	auto plane = m_Planes[m_Nodes[node_id].plane];
	SplitPolygon(portal_poly, polygons, plane);

	portal = nullptr;
	uint32_t i = 0;
	glm::vec3 p00(polygons[0]->m_Vertices[i], polygons[i]->m_Vertices[i + 1], polygons[0]->m_Vertices[i + 2]);
	float dot = glm::dot(plane.n, p00 - plane.p0);
	while (Cmpf(dot, 0.0f))
	{
		i += 3;
		if (i >= polygons[0]->m_Vertices.size())
			break;
		p00 = glm::vec3(polygons[0]->m_Vertices[i], polygons[0]->m_Vertices[i + 1], polygons[0]->m_Vertices[i + 2]);
		dot = glm::dot(plane.n, p00 - plane.p0);
	}

	if (dot > 0.f)
	{
		front_portal->portal = *polygons[0];
		back_portal->portal = *polygons[1];
	}
	else
	{
		front_portal->portal = *polygons[1];
		back_portal->portal = *polygons[0];
	}


	if (m_Nodes[node_id].is_leaf)
	{
		front_portal->leafs[front_portal->leafs_amount] = m_Nodes[node_id].front;
		front_portal->leafs_amount++;
		front_portals.push_back(front_portal);
	}
	else
	{
		front_portals = ClipPortal(front_portal, m_Nodes[node_id].front);
	}

	if (m_Nodes[node_id].back == -1)
	{
		portal = nullptr;
	}
	else
	{
		back_portals = ClipPortal(back_portal, m_Nodes[node_id].back);
	}

	for (const auto& p : back_portals)
	{
		front_portals.push_back(p);
	}
	return front_portals;
}

std::vector<std::shared_ptr<Portal>> PVSTree::ClipOnBackPortal(uint32_t node_id, std::shared_ptr<Portal> portal)
{
	std::vector<std::shared_ptr<Portal>> back_portals;
	if (m_Nodes[node_id].back == -1)
	{
		portal = nullptr;  //if it was the only reference its equal to deletion
		return back_portals;
	}
	else
	{
		return ClipPortal(portal, m_Nodes[node_id].back);
	}
}

std::vector<std::shared_ptr<Portal>> PVSTree::ClipInFrontPortal(uint32_t node_id, std::shared_ptr<Portal> portal)
{
	std::vector<std::shared_ptr<Portal>> front_portals;
	if (m_Nodes[node_id].is_leaf)
	{
		portal->leafs[portal->leafs_amount] = m_Nodes[node_id].front;
		portal->leafs_amount++;
		front_portals.push_back(portal);
		return front_portals;
	}
	else
	{
		return ClipPortal(portal, m_Nodes[node_id].front);
	}
}

std::vector<std::shared_ptr<Portal>> PVSTree::ClipOnPlanePortal(uint32_t node_id, std::shared_ptr<Portal> portal)
{
	std::vector<std::shared_ptr<Portal>> front_portals, back_portals;
	if (m_Nodes[node_id].is_leaf)
	{
		portal->leafs[portal->leafs_amount] = m_Nodes[node_id].front;
		portal->leafs_amount++;
		front_portals.push_back(portal);
	}
	else
	{
		front_portals = ClipPortal(portal, m_Nodes[node_id].front);
	}

	//if nothing survived front clipping or there is no back, return the results
	if (front_portals.empty() || m_Nodes[node_id].back == -1)
		return front_portals;

	for (const auto& port : front_portals)
	{
		auto result = ClipPortal(port, m_Nodes[node_id].back);
		for (const auto& r : result)
		{
			back_portals.push_back(r);
		}
	}
	return back_portals;
}

bool PVSTree::IsPortalDuplicate(std::shared_ptr<Portal> portal, int32_t* index)
{
	uint32_t leaf1 = portal->leafs[0];
	uint32_t leaf2 = portal->leafs[1];

	uint32_t curr_leaf1;
	uint32_t curr_leaf2;

	for (uint32_t i = 0; i < m_Portals.size(); ++i)
	{
		curr_leaf1 = m_Portals[i]->leafs[0];
		curr_leaf2 = m_Portals[i]->leafs[1];
		if ((curr_leaf1 == leaf1 && curr_leaf2 == leaf2)
			|| (curr_leaf2 == leaf1 && curr_leaf1 == leaf2))
		{
			BBox old_box;
			BBox new_box;
			CalculateBBox(&old_box, std::make_shared<Polygon>(m_Portals[i]->portal));
			CalculateBBox(&new_box, std::make_shared<Polygon>(portal->portal));
			float new_size = static_cast<float>((new_box.min - new_box.min).length());
			float old_size = static_cast<float>((old_box.min - old_box.min).length());

			if (new_size > old_size)
			{
				*index = i;
				return true;
			}
			else
			{
				return true;
			}
		}
	}
	return false;

}

std::shared_ptr<Portal> PVSTree::CalculatePortal(uint32_t node_id)
{
	return CalculatePortal(m_Nodes[node_id]);
}

std::shared_ptr<Portal> PVSTree::CalculatePortal(const PVSNode& node)
{
	glm::vec3 min = node.bounding_box.min;
	glm::vec3 max = node.bounding_box.max;
	glm::vec3 normal = m_Planes[node.plane].n;

	glm::vec3 box_center = (max + min) / 2.f;
	float distance_to_plane = glm::dot(m_Planes[node.plane].p0 - box_center, normal);
	glm::vec3 portal_center = box_center + (normal * distance_to_plane);

	glm::vec3 a(0.f);
	if (fabs(normal.y) > fabs(normal.z))
	{
		if (fabs(normal.z) < fabs(normal.x))
		{
			a.z = 1;
		}
		else
		{
			a.x = 1;
		}
	}
	else
	{
		if (fabs(normal.y) <= fabs(normal.x))
		{
			a.y = 1;
		}
		else
		{
			a.x = 1;
		}
	}
	glm::vec3 u = glm::cross(a, normal);
	u = glm::normalize(u);
	glm::vec3 v = glm::cross(u, normal);
	v = glm::normalize(v);

	glm::vec3 half_box_vector = max - box_center;
	u = 2.f * u * static_cast<float>(half_box_vector.length());
	v = 2.f * v * static_cast<float>(half_box_vector.length());
	glm::vec3 vertices[4];
	vertices[0] = portal_center + u - v;
	vertices[1] = portal_center + u + v;
	vertices[2] = portal_center - u + v;
	vertices[3] = portal_center - u - v;
	std::shared_ptr<Portal> portal = std::make_shared<Portal>();
	for (const auto& v : vertices)
	{
		portal->portal.m_Vertices.push_back(v.x);
		portal->portal.m_Vertices.push_back(v.y);
		portal->portal.m_Vertices.push_back(v.z);
	}
	return portal;
}

uint32_t PVSTree::GetBestSplitter(polygon_list& polygons, polygon_list& frontPolygons)
{
	//making a mock tree build for each polygon to see how good splitter he is

	std::vector<uint32_t> polygon_scores;
	for (const auto& poly : polygons)
	{
		if (poly->IsSplitter())
		{
			polygon_scores.push_back(std::numeric_limits<uint32_t>::max());
			continue;
		}

		Plane plane(*poly);
		int front_count = 0;
		int back_count = 0;
		int split_count = 0;

		for (const auto& p2 : polygons)
		{
			std::vector<glm::vec3> front;
			std::vector<glm::vec3> back;
			PositionVerticesToPlane(p2->m_Vertices, p2->GetParent()->GetTransform(), plane, front, back);
			if (front.empty()) //polygon on the backside
			{
				++back_count;
			}
			else if (back.empty()) //polygon on the front
			{
				++front_count;
			}
			else //need to split it 
			{
				++split_count;
				//no need to increment other counters as we plan to take a difference of them anyway
			}
		}
		polygon_scores.push_back(std::abs(front_count - back_count) + split_count * 3);
	}

	uint32_t best_index = 0;
	uint32_t best_score = polygon_scores[0];
	for (uint32_t i = 1; i < polygon_scores.size(); ++i)
	{
		if (polygon_scores[i] < best_score)
		{
			best_index = i;
			best_score = polygon_scores[i];
		}
	}
	polygons[best_index]->IsSplitter(true);
	m_Planes.emplace_back(*polygons[best_index]);
	frontPolygons.push_back(polygons[best_index]);
	polygons.erase(polygons.begin() + best_index);
	return m_Planes.size() - 1;
}
