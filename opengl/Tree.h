#pragma once
#include <vector>
#include <memory>
#include <tuple>
#include <queue>
#include "Plane.h"

class Polygon;
class Object;
class Renderer;

using polygon_list = std::vector<std::shared_ptr<Polygon>>;
using poly_tuple = std::tuple<std::shared_ptr<Polygon>, std::shared_ptr<Polygon>, std::shared_ptr<Polygon>>;

struct Node
{
	Node();
	Node(std::shared_ptr<Polygon> polygon);
	~Node();

	Node* front = nullptr; 
	Node* back = nullptr; 

	polygon_list polygons;
	Plane plane;
};


class Tree
{
	friend class Renderer;
public:
	Tree() = default;;
	Tree(polygon_list polygons);
	~Tree();

protected:
	void MakeTree(Node* node, polygon_list& polygons);
	
	void PositionPolygonToPlane(std::vector<glm::vec3>& frontVertices, std::vector<glm::vec3>& backVertices, const Plane& plane, std::shared_ptr<Polygon> poly, polygon_list& backPolygons, polygon_list &frontPolygons, std::queue<poly_tuple>& polygonsToReplace);
	int8_t PositionVerticesToPlane(const std::vector<float> &vertices, const glm::mat4& transform, const Plane& plane, std::vector<glm::vec3> &front, std::vector<glm::vec3> &back);
	int8_t PositionVerticesToPlane(const std::vector<float> &vertices, const Plane& plane);
	int8_t PositionVerticesToPlane(const std::vector<glm::vec3> &vertices, const Plane& plane);
	int8_t PositionVertexToPlane(const glm::vec3& vertex, const Plane& plane);
	void AddQueuedPolygonsToObjects(std::queue<poly_tuple> &polygonsToReplace);
	void SplitPolygon(std::shared_ptr<Polygon> poly, std::array<std::shared_ptr<Polygon>, 2>& results, const Plane& plane);
	void AddColorToPolygon(std::shared_ptr<Polygon> polygon, glm::vec4& colors);
	void AddVertexToPolygon(std::shared_ptr<Polygon> polygon, glm::vec3& vertex);
	
	glm::vec4 InterpolateColor(const glm::vec3& interpolationPoint, const glm::vec3& beginPointPos, const glm::vec3& endPointPos, const glm::vec4& beginPointColor, const glm::vec4& endPointColor);

	Node* m_Root = nullptr;
};


void AddSplitPolygonsToObject(Object* object, std::shared_ptr<Polygon> currentPolygon, std::shared_ptr<Polygon> p1, std::shared_ptr<Polygon> p2);