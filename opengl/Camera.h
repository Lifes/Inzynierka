#pragma once
#include "glm.hpp"
#include "Transform.h"
#include "Plane.h"
#include <array>

class Camera
{
public:
	Camera(float fov = 90.f, float near = 0.1f, float far = 100.f);
	Camera(float width, float heigth, float fov = 90.f, float near = 0.1f, float far = 100.f);

	void Update(float dt);

	void SetFov(float fov);
	void SetNearPlane(float near);
	void SetFarPlane(float far);
	void SetResolution(float width, float height);

	glm::mat4 GetViewProjectionMatrix() { return m_Projection * m_Transform.GetTransformMatrix(); }
	glm::vec3 GetPosition();
	const std::array<Plane, 6>& GetFrustumArray() const { return m_Frustum; }

private:
	void SetProjectionMatrix();
	void SetViewMatrix();
	void UpdateFrustum();

	glm::mat4 m_Projection;
	//camera transform aka view matrix
	CameraTransform m_Transform; 
	std::array<Plane, 6> m_Frustum;

	float m_MovementVelocity = 10.f;
	float m_AngularVelocity = 0.01f;

	float m_FOV = 90.f;
	float m_ResWitdh = 1280.f;
	float m_ResHeight = 720.f;
	float m_Near = 0.1f;
	float m_Far = 100.f;
};