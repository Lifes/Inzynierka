#include "Plane.h"
#include "Object.h"
#include "Float.h"
#include "glm.hpp"

Plane::Plane()
	: n(0.f)
	, p0(0.f)
{
}

Plane::Plane(const Polygon& polygon)
	: n(0.f)
{
	//3d game primer listing 9.5
	const auto& vertices = polygon.m_Vertices;
	auto size = vertices.size();

	glm::vec3 p (vertices[size - 3], vertices[size - 2], vertices[size - 1]);
	auto transform = polygon.GetParent()->GetTransform();

	//set p0 to a point we know is on the plane
	glm::vec3 ws_p = static_cast<glm::vec3>(transform  * glm::vec4(p, 1.f));
	p0 = ws_p;

	for (uint32_t i = 0; i + 2 < vertices.size(); i += 3)
	{
		glm::vec3 c(vertices[i], vertices[i + 1], vertices[i + 2]);
		glm::vec3 ws_c = static_cast<glm::vec3>(transform * glm::vec4(c, 1.f));
		n.x += (ws_p.z + ws_c.z) * (ws_p.y - ws_c.y);
		n.y += (ws_p.x + ws_c.x) * (ws_p.z - ws_c.z);
		n.z += (ws_p.y + ws_c.y) * (ws_p.x - ws_c.x);

		ws_p = ws_c;
	}
	n = glm::normalize(n);
}

//creates plane for set of world space vertices
Plane::Plane(const std::vector<glm::vec3>& vertices)
	:n(0.f)
{
	auto size = vertices.size();
	glm::vec3 p = vertices[size - 1];

	//set p0 to a point we know is on the plane
	p0 = p;

	for (glm::vec3 c : vertices)
	{
		n.x += (p.z + c.z) * (p.y - c.y);
		n.y += (p.x + c.x) * (p.z - c.z);
		n.z += (p.y + c.y) * (p.x - c.x);

		p = c;
	}
	n = glm::normalize(n);
}

Plane::Plane(const std::vector<float>& vertices)
	:n(0.f)
{
	auto size = vertices.size();

	glm::vec3 p(vertices[size - 3], vertices[size - 2], vertices[size - 1]);

	//set p0 to a point we know is on the plane
	p0 = p;

	for (uint32_t i = 0; i + 2 < vertices.size(); i += 3)
	{
		glm::vec3 c(vertices[i], vertices[i + 1], vertices[i + 2]);
		n.x += (p.z + c.z) * (p.y - c.y);
		n.y += (p.x + c.x) * (p.z - c.z);
		n.z += (p.y + c.y) * (p.x - c.x);

		p = c;
	}
	n = glm::normalize(n);
}


Plane::~Plane()
{
}

int FindPlaneLineIntersection(const Plane& plane, const glm::vec3& line_p0, const glm::vec3& line_p1, glm::vec3* result)
{
	//line representation as p(t) = p0 + td
	glm::vec3 d = line_p1 - line_p0;
	glm::vec3 w = line_p0 - plane.p0;

	//t = -nw / nu

	float nu = glm::dot(plane.n, d);
	float mnw = -glm::dot(plane.n, w);

	if (Cmpf(nu, 0.f))
	{
		if (Cmpf(mnw, 0.f))
			return 2;
		else
			return 0;
	}

	float t = mnw / nu;
	*result = line_p0 + t * d; //p(t)

	if (t < 0 || t > 1)
		return 1;
	return 2;
}
