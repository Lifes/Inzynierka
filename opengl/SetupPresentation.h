#pragma once


Scene* SetupBasicScene();
Scene* SetupSimpleLargeScene();
Scene* SetupConvexScene(int roomsAmount);