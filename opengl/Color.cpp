#include "Color.h"

Color Color::RED()
{
	return Color(1.0f, 0.0f, 0.0f);
}

Color Color::GREEN()
{
	return Color(0.0f, 1.0f, 0.0f);
}

Color Color::BLUE()
{
	return Color(0.0f, 0.0f, 1.0f);
}

Color Color::BLACK()
{
	return Color(0.0f, 0.0f, 0.0f);
}

Color Color::WHITE()
{
	return Color(1.0f, 1.0f, 1.0f);
}

Color::Color()
	: m_Red(0.0f)
	, m_Green(0.0f)
	, m_Blue(0.0f)
	, m_Alpha(1.0f)
{
}

Color::Color(float red, float green, float blue, float alpha)
	: m_Red(red)
	, m_Green(green)
	, m_Blue(blue)
	, m_Alpha(alpha)
{
}

Color::~Color()
{
}
