#pragma once
#include "glm.hpp"
#include "glew.h"
#include "Tree.h"
#include "Transform.h"

#include <memory>
#include <vector>
#include <string>

class Polygon;

class Object
{
	friend void AddSplitPolygonsToObject(Object* object, std::shared_ptr<Polygon> currentPolygon, std::shared_ptr<Polygon> p1, std::shared_ptr<Polygon> p2);
public:
	Object(const std::string& pathToPositions, const std::string& pathToColors, bool isStatic = false);
	Object(const Object& obj);
	~Object();

	const glm::mat4& GetTransform() const;
	const std::vector<std::shared_ptr<Polygon>>& GetPolygons() const{ return m_Polygons; }

	void Translate(const glm::vec3& rhs);
	void Rotate(const glm::vec3& rhs);
	void Rescale(const glm::vec3& rhs);

	void LoadFromFile(const std::string& pathToVertices, const std::string& pathToColors);

	//for easier debugging
	int ID = 0;
	int poly_count = 0;
private:
	void SetBuffers();

	std::vector<std::shared_ptr<Polygon>> m_Polygons;

	Transform m_Transform;

	bool m_IsStatic;
};

class Polygon
{
public:
	Polygon(Object* parent = nullptr);
	~Polygon();
	
	Object* GetParent() const { return m_Parent; }

	bool IsSplitter() const { return m_IsSplitter; }
	void IsSplitter(bool val) { m_IsSplitter = val; }

	void Render(bool wireframe = false) const;
	void SetBuffers();

	std::vector<float> m_Vertices;
	std::vector<float> m_Colors;

	//for easier debugging
	//local to parent object
	int ID = 0;

private:
	Object* m_Parent;

	GLuint m_VBO[2];
	GLuint m_VAO;
	GLuint m_PositionID = 0;
	GLuint m_ColorID = 1;
	bool m_IsSplitter;
};