#include "Shader.h"

#include "Libs/glew/glew.h"
#include <SDL.h>

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>

Shader::Shader(const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
	: m_VertexShaderPath(vertexShaderPath)
	, m_FragmentShaderPath(fragmentShaderPath)
{
	m_Program = glCreateProgram();
	glBindAttribLocation(m_Program, POSITION_ATT_LOCATION, "in_Position");
	glBindAttribLocation(m_Program, COLOR_ATT_LOCATION, "in_Color");

	LoadVertexShader();
	LoadFragmentShader();
	LinkShaders();
}

Shader::~Shader()
{
	glUseProgram(0);
	glDetachShader(m_Program, m_VertexShader);
	glDetachShader(m_Program, m_FragmentShader);
	glDeleteProgram(m_Program);
	glDeleteShader(m_VertexShader);
	glDeleteShader(m_FragmentShader);
}

void Shader::Run()
{
	glUseProgram(m_Program);
}

void Shader::ReloadVertexShader(const std::string& vertexShaderPath)
{
	m_VertexShaderPath = vertexShaderPath;
	glDetachShader(m_Program, m_VertexShader);
	LoadVertexShader();
	LinkShaders();
}

void Shader::ReloadFragmentShader(const std::string& fragmentShaderPath)
{
	m_FragmentShaderPath = fragmentShaderPath;
	glDetachShader(m_Program, m_FragmentShader);
	LoadFragmentShader();
	LinkShaders();
}

void Shader::SetMVPMatrix(const glm::mat4& mvp)
{
	auto id = glGetUniformLocation(m_Program, "mvp");
	glUniformMatrix4fv(id, 1, GL_FALSE, &mvp[0][0]);
}

//Returns whole contents of a file as string
std::string Shader::ReadFile(const std::string& path) const
{
	std::ifstream in(path);
	std::stringstream buffer;
	buffer << in.rdbuf();
	return buffer.str();
}

bool Shader::LoadVertexShader()
{
	std::string shader_src = ReadFile(m_VertexShaderPath);
	char* src = const_cast<char*>(shader_src.c_str());
	int length = shader_src.length();

	m_VertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(m_VertexShader, 1, &src, &length);
	glCompileShader(m_VertexShader);

	int compiled = 0;
	glGetShaderiv(m_VertexShader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		PrintDebugMessage(m_VertexShader);
		return false;
	}
	glAttachShader(m_Program, m_VertexShader);
	return true;
}

bool Shader::LoadFragmentShader()
{
	if (m_FragmentShaderPath.empty())
		return false;

	std::string shader_src = ReadFile(m_FragmentShaderPath);
	char* src = const_cast<char*>(shader_src.c_str());
	int length = shader_src.length();

	m_FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(m_FragmentShader, 1, &src, &length);
	glCompileShader(m_FragmentShader);

	int compiled = 0;
	glGetShaderiv(m_FragmentShader, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		PrintDebugMessage(m_FragmentShader);
		return false;
	}
	glAttachShader(m_Program, m_FragmentShader);
	return true;
}

bool Shader::LinkShaders()
{
	glLinkProgram(m_Program);
	int isLinked;
	glGetProgramiv(m_Program, GL_LINK_STATUS, &isLinked);

	if (!isLinked)
	{
		std::cerr << "\nShader linking error\n";
#ifdef DEBUG
		int log_length;
		glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &log_length);

		char* log = new char[maxLength];
		glGetProgramInfoLog(shaderProgram, log_length, &log_length, log);

		std::cerr << "Error message : " << log << "\n";

		delete[] shaderProgramInfoLog;
#endif // DEBUG
	}

	return isLinked != 0;
}

void Shader::PrintDebugMessage(int shaderId) const
{
	std::cerr << "\nShader compilation error\n";
#ifdef DEBUG
	int message_length;
	glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &message_length);
	char* log = new char[message_length];
	glGetShaderInfoLog(shaderId, message_length, &message_length, log);

	std::cerr << "ERROR LOG:\n" << log << "\n\n";
	delete[] log;
#endif // DEBUG
}