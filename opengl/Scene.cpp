#include "Scene.h"

Scene::~Scene()
{
	for (auto ob : m_Objects)
		if (ob != nullptr)
			delete ob;
}

void Scene::AddObject(const std::string& pathToPositions, const std::string& pathToColors)
{
	Object* obj = new Object(pathToPositions, pathToColors);
	m_Objects.push_back(obj);
	m_PolygonsDirty = true;
}

void Scene::AddObject(const Object& object)
{
	Object* obj = new Object(object);
	m_Objects.push_back(obj);
	m_PolygonsDirty = true;
}

void Scene::UpdatePolygonList()
{
	if (!m_PolygonsDirty)
		return;

	m_Polygons.clear();
	for (auto&& ob : m_Objects)
	{
		for (auto&& poly : ob->GetPolygons())
		{
			m_Polygons.push_back(poly);
		}
	}

	m_SceneTree = std::make_shared<Tree>(m_Polygons);

	m_PvsSceneTree = std::make_shared<PVSTree>(m_Polygons);


	m_PolygonsDirty = false;
}

