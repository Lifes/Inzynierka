#include "Object.h"
#include "Render.h"
#include "Shader.h"
#include "Transform.h"

#include "glm.hpp"
#include "gtc\matrix_transform.hpp"
#include "gtc\quaternion.hpp"
#include "gtx\quaternion.hpp"
#include "glew.h"

#include <memory>
#include <fstream>

static int object_count = 1;
static constexpr float DEG_TO_RAD = 0.01745329252f;

Object::Object(const std::string& pathToPositions, const std::string& pathToColors, bool isStatic)
	: m_Transform()
	, m_IsStatic(isStatic)
{
	ID = object_count++;
	LoadFromFile(pathToPositions, pathToColors);
}

Object::Object(const Object& obj)
	: m_IsStatic(obj.m_IsStatic)
{
	ID = object_count++;
	for (const auto& poly : obj.m_Polygons)
	{
		m_Transform = obj.m_Transform;
		auto new_poly = std::make_shared<Polygon>(this);
		new_poly->m_Vertices = poly->m_Vertices;
		new_poly->m_Colors = poly->m_Colors;
		new_poly->SetBuffers();
		m_Polygons.push_back(new_poly);
	}
}

Object::~Object()
= default;

const glm::mat4& Object::GetTransform() const
{
	return m_Transform.GetTransformMatrix();
}

void Object::Translate(const glm::vec3& rhs)
{
	if (m_IsStatic)
		return;

	m_Transform.Translate(rhs);
}

void Object::Rotate(const glm::vec3& euler)
{
	if (m_IsStatic)
		return;

	glm::vec3 euler_in_rad = euler * DEG_TO_RAD;

	glm::quat rot = glm::quat(euler_in_rad);
	m_Transform.Rotate(rot);
}

void Object::Rescale(const glm::vec3& rhs)
{
	if (m_IsStatic)
		return; 

	m_Transform.SetScale(rhs);
}

void Object::LoadFromFile(const std::string& pathToVertices, const std::string& pathToColors)
{
	using namespace std;
	ifstream in(pathToVertices);

	if (!in.is_open())
		return;

	float val;

	//read objects vertices
	std::string line;

	auto poly = std::make_shared<Polygon>(this);
	m_Polygons.push_back(poly);
	std::string delimiter = " ";

	while (getline(in, line))
	{
		if (line.empty()){
			poly = std::make_shared<Polygon>(this);
			m_Polygons.push_back(poly);
		}
		else
		{
			std::string substr;
			int pos = 0;
			while ((pos = line.find(delimiter)) != std::string::npos)
			{
				substr = line.substr(0, pos);
				val = stof(substr);
				poly->m_Vertices.push_back(val);
				line.erase(0, pos + delimiter.length());
			}
			val = stof(line);
			poly->m_Vertices.push_back(val);
		}
	}
	in.get();
	in.close();
	in.open(pathToColors);

	if (!in.is_open())
		return;

	auto it = m_Polygons.begin();
	while (getline(in, line) && it != m_Polygons.end())
	{
		if (line.empty())
		{
			++it;
		}
		else
		{
			std::string substr;
			int pos = 0;
			while ((pos = line.find(delimiter)) != std::string::npos)
			{
				substr = line.substr(0, pos);
				val = stof(substr);
				(*it)->m_Colors.push_back(val);
				line.erase(0, pos + delimiter.length());
			}
			val = stof(line);
			(*it)->m_Colors.push_back(val);
		}
	}
	in.get();
	in.close();
	SetBuffers();
}

void Object::SetBuffers()
{
	for (auto& poly : m_Polygons)
	{
		poly->SetBuffers();
	}
}


////////////////////////////////////////////////////////

Polygon::Polygon(Object* parent)
	: m_Parent(parent)
	, m_IsSplitter(false)
{
	if (parent != nullptr)
		ID = ++(parent->poly_count);
}

Polygon::~Polygon()
{
	glDeleteBuffers(2, m_VBO);
	glDeleteVertexArrays(1, &m_VAO);
}


void Polygon::Render(bool wireframe /*= false*/) const
{
	glBindVertexArray(m_VAO);

	if(wireframe)
		glDrawArrays(GL_LINE_LOOP, 0, m_Vertices.size() / 3);
	else
		glDrawArrays(GL_POLYGON, 0, m_Vertices.size() / 3);
}

void Polygon::SetBuffers()
{
	glGenBuffers(2, m_VBO);
	glGenVertexArrays(1, &m_VAO);

	glBindVertexArray(m_VAO);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO[0]);
	glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(float), &m_Vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(m_PositionID, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(m_PositionID);

	if (!m_Colors.empty())
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_VBO[1]);
		glBufferData(GL_ARRAY_BUFFER, m_Colors.size()* sizeof(float) , &m_Colors[0], GL_STATIC_DRAW);
		glVertexAttribPointer(m_ColorID, 4, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(m_ColorID);
	}

	glBindVertexArray(m_VAO);
}
