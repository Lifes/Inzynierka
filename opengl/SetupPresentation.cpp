#include "Scene.h"
#include <cstdlib>
#include <ctime>



Scene* SetupBasicScene()
{
	Scene* scene = new Scene();
	Object ob("../objects/ob1.ob", "../objects/ob1.cl");
	Object ob1(ob);
	Object ob2(ob);
	Object ob3(ob);

	ob.Rescale(glm::vec3(2, 2, 2));
	ob1.Translate(glm::vec3(0, 0, -5));
	ob2.Translate(glm::vec3(4, 0, 0));
	ob3.Translate(glm::vec3(4, 2, -5));
	ob3.Rescale(glm::vec3(3, 3, 3));
	ob3.Rotate(glm::vec3(40, 30, 0.f));

	scene->AddObject(ob);
	scene->AddObject(ob1);
	scene->AddObject(ob2);
	scene->AddObject(ob3);
	scene->UpdatePolygonList();
	return scene;
}

Scene* SetupSimpleLargeScene()
{
	std::srand(std::time(nullptr));
	int row_length = 5;
	Scene* scene = new Scene();
	Object ob("../objects/ob1.ob", "../objects/ob1.cl");
	Object ob2(ob);
	ob2.Translate(glm::vec3(4, 0, 0));

	scene->AddObject(ob2);

	for (int i = 0; i < row_length; ++i)
	{
		for (int j = 0; j < row_length; ++j)
		{
			for (int k = 0; k < row_length; ++k)
			{
				Object ob1(ob);
				ob1.Translate(glm::vec3(10 * k, 10 * i, 10 * j));
				int size = std::rand() % 6 + 1;
				//ob1.Rescale(glm::vec3(size));
				scene->AddObject(ob1);
			}
		}
	}

	scene->UpdatePolygonList();
	Object square1("../objects/square.ob", "../objects/square.cl");
	return scene;
}

Scene* SetupConvexScene(int roomsAmount)
{


	Scene* scene = new Scene();
	//TODO add all to vector and make more rooms
	Object* room[7];
	Object front_wall("../objects/ob1.ob", "../objects/ob1.cl");
	Object mid_cube(front_wall);
	mid_cube.Translate(glm::vec3(0, -3, 5));
	mid_cube.Rotate(glm::vec3(45, 135, 45));
	front_wall.Rescale(glm::vec3(10, 10, 1));

	Object back_left_wall(front_wall);
	back_left_wall.Rescale(glm::vec3(4, 10, 1));
	back_left_wall.Translate(glm::vec3(-3, 0, 9));

	Object top_wall(front_wall);
	top_wall.Rotate(glm::vec3(90, 0, 0));
	top_wall.Translate(glm::vec3(0, 5.5, 4.5));
	
	Object bottom_wall(top_wall);
	bottom_wall.Translate(glm::vec3(0, -11, 0));

	Object back_right_wall(back_left_wall);
	Object right_wall(back_left_wall);
	back_right_wall.Translate(glm::vec3(6, 0, 0));
	right_wall.Rotate(glm::vec3(0, 90, 0));
	right_wall.Translate(glm::vec3(8.5, 0, -2.5));

	room[0] = &front_wall;
	room[1] = &mid_cube;
	room[2] = &back_right_wall;
	room[3] = &back_left_wall;
	room[4] = &right_wall;
	room[5] = &top_wall;
	room[6] = &bottom_wall;

	for (int i = 0; i < roomsAmount; ++i)
	{
		for (auto ob : room)
		{
			if (i != 0)
				ob->Translate(glm::vec3(10, 0, 0));
			scene->AddObject(*ob);
		}
	}
	for (auto ob : room)
	{
		ob->Translate(glm::vec3(0, 0, 10));
	}
	room[0] = nullptr;
	for (int i = 0; i < roomsAmount; ++i)
	{
		for (auto ob : room)
		{
			if(ob == nullptr)
				continue;
			if(i != 0)
				ob->Translate(glm::vec3(-10, 0, 0));
			scene->AddObject(*ob);
		}
	}





	return scene;

}