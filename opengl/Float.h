#pragma once
#include <algorithm>

constexpr float FLOAT_EPS = 1e-4f;

static constexpr float Abs(float n) { return n < 0.0f ? -n : n; }

inline constexpr bool Cmpf(float a, float b, float eps = FLOAT_EPS)
{
	return Abs(a - b) <= (eps * std::max(std::max(1.0f, Abs(a)), Abs(b)));
}
