#include "Tree.h"
#include "Object.h"
#include "Float.h"
#include "glm.hpp"
#include "Render.h"
#include "gtc\matrix_transform.hpp"

#include <queue>
#include <tuple>
#include <array>


Tree::Tree(polygon_list polygons)
{
	m_Root = new Node(polygons.back());
	polygons.pop_back();
	MakeTree(m_Root, polygons);
}

Tree::~Tree()
{
	if (m_Root == nullptr)
		delete m_Root;
}

void Tree::MakeTree(Node* node, polygon_list& polygons)
{

	polygon_list front_polygons;
	polygon_list back_polygons;

	std::queue<poly_tuple> polygons_to_replace;

	for (const auto& poly : polygons)
	{
		auto transform = poly->GetParent()->GetTransform();
		const auto& vertices = poly->m_Vertices;
		std::vector<glm::vec3> front;
		std::vector<glm::vec3> back;

		PositionVerticesToPlane(vertices, transform, node->plane, front, back);
		PositionPolygonToPlane(front, back, node->plane, poly, back_polygons, front_polygons, polygons_to_replace);
	}

	AddQueuedPolygonsToObjects(polygons_to_replace);

	if (!front_polygons.empty())
	{
		node->front = new Node(front_polygons.back());
		front_polygons.pop_back();
		MakeTree(node->front, front_polygons);
	}
	if (!back_polygons.empty())
	{
		node->back = new Node(back_polygons.back());
		back_polygons.pop_back();
		MakeTree(node->back, back_polygons);
	}
}

void Tree::PositionPolygonToPlane(std::vector<glm::vec3>& frontVertices, std::vector<glm::vec3>& backVertices, const Plane& plane, std::shared_ptr<Polygon> poly, polygon_list& backPolygons, polygon_list &frontPolygons, std::queue<poly_tuple>& polygonsToReplace)
{

	if (frontVertices.empty() && backVertices.empty()) //polygon fully on plane
	{
		//node->polygons.push_back(poly);
		Plane polygon_plane(*poly);
		if (glm::dot(polygon_plane.n, plane.n) > 0.f)
			frontPolygons.push_back(poly);
		else
			backPolygons.push_back(poly);
	}
	else if (frontVertices.empty()) //polygon on the backside
	{
		backPolygons.push_back(poly);
	}
	else if (backVertices.empty()) //polygon on the front
	{
		frontPolygons.push_back(poly);
	}
	else
	{
		Object* parent = poly->GetParent();
		std::array<std::shared_ptr<Polygon>, 2> results;
		results[0] = std::make_shared<Polygon>(parent);
		results[1] = std::make_shared<Polygon>(parent);
		results[0]->IsSplitter(poly->IsSplitter());
		results[1]->IsSplitter(poly->IsSplitter());

		SplitPolygon(poly, results, plane);

		results[0]->SetBuffers();
		results[1]->SetBuffers();

		//queue split polygons to be added to parent instead of original
		//AddSplitPolygonsToObject(parent, poly, results[0], results[1]);
		poly_tuple tuple = std::make_tuple(poly, results[0], results[1]);
		polygonsToReplace.push(tuple);

		//check which polygon is on which side
		//check just first vertex of first polygon
		//if its on the front, the whole polygon will be on front
		//and the other on the back
		uint32_t i = 0;
		glm::vec3 p00(results[0]->m_Vertices[i], results[i]->m_Vertices[i+1], results[0]->m_Vertices[i+2]);
		p00 = static_cast<glm::vec3>(parent->GetTransform() * glm::vec4(p00, 1.f));
		float dot = glm::dot(plane.n, p00 - plane.p0);
		while (Cmpf(dot, 0.0f))
		{
			i += 3;
			if (i >= results[0]->m_Vertices.size())
				break;
			p00 = glm::vec3(results[0]->m_Vertices[i], results[0]->m_Vertices[i + 1], results[0]->m_Vertices[i + 2]);
			dot = glm::dot(plane.n, p00 - plane.p0);
		}

		if (dot > 0.f)
		{
			frontPolygons.push_back(results[0]);
			backPolygons.push_back(results[1]);
		}
		else
		{
			frontPolygons.push_back(results[1]);
			backPolygons.push_back(results[0]);
		}
	}
}

int8_t Tree::PositionVerticesToPlane(const std::vector<float> &vertices, const glm::mat4& transform, const Plane& plane, std::vector<glm::vec3> &front, std::vector<glm::vec3> &back)
{
	for (uint32_t i = 0; i + 2 < vertices.size(); i += 3)
	{
		glm::vec4 os_vertex(vertices[i], vertices[i + 1], vertices[i + 2], 1.0f);
		glm::vec3 ws_vertex = static_cast<glm::vec3>(transform * os_vertex);
		auto dot = glm::dot(plane.n, ws_vertex - plane.p0);
		if (Cmpf(dot, 0.f))
		{
			continue; //on plane, do nothing
		}
		else if (dot > 0.f) //in front
		{
			front.push_back(ws_vertex);
		}
		else //in back
		{
			back.push_back(ws_vertex);
		}
	}

	if (front.empty() && back.empty())
		return 0;
	else if (front.empty())
		return -1;
	else
		return 1;
}


int8_t Tree::PositionVerticesToPlane(const std::vector<float> &vertices, const Plane& plane)
{
	int32_t front = 0, back = 0;
	for (uint32_t i = 0; i + 2 < vertices.size(); i += 3)
	{
		glm::vec3 ws_vertex(vertices[i], vertices[i + 1], vertices[i + 2]);
		int8_t result = PositionVertexToPlane(ws_vertex, plane);
		if (result == 1)
			++front;
		else if (result == -1)
			++back;
	}
	if (front != 0 && back != 0)
		return 2;
	else if (front != 0)
		return 1;
	else if (back != 0)
		return -1;
	else
		return 0;
}

int8_t Tree::PositionVerticesToPlane(const std::vector<glm::vec3> &vertices, const Plane& plane)
{
	int32_t front = 0, back = 0;
	for (glm::vec3 vec : vertices)
	{
		int8_t result = PositionVertexToPlane(vec, plane);
		if (result == 1)
			++front;
		else if (result == -1)
			++back;
	}
	if (front != 0 && back != 0)
		return 2;
	else if (front != 0)
		return 1;
	else if (back != 0)
		return -1;
	else
		return 0;
}

//returns 0 if on plane, 1 if in front, -1 if in back
int8_t Tree::PositionVertexToPlane(const glm::vec3& vertex, const Plane& plane)
{
	auto dot = glm::dot(plane.n, vertex - plane.p0);
	if (Cmpf(dot, 0.f))
		return 0;
	else if (dot > 0.f)
		return 1;
	else
		return -1;
}

void Tree::AddQueuedPolygonsToObjects(std::queue<poly_tuple>& polygonsToReplace)
{
	while (!polygonsToReplace.empty())
	{
		poly_tuple tuple = polygonsToReplace.front();
		AddSplitPolygonsToObject(std::get<0>(tuple)->GetParent(), std::get<0>(tuple), std::get<1>(tuple), std::get<2>(tuple));
		polygonsToReplace.pop();
	}
}

void Tree::SplitPolygon(std::shared_ptr<Polygon> poly, std::array<std::shared_ptr<Polygon>, 2>& results, const Plane& plane)
{
	const auto& vertices = poly->m_Vertices;
	auto parent = poly->GetParent();
	const glm::mat4& transform = parent == nullptr ? glm::mat4(1) : parent->GetTransform();
	int size = vertices.size();
	int active_poly = 0;
	glm::mat4 inv_transform = glm::inverse(transform);
	
	//store both object & world space coordinates
	//OS are pushed to new polygons
	//ws are used for calculations
	const auto& colors = poly->m_Colors;
	uint32_t colors_size = colors.size();
	glm::vec4 colors0;
	if (colors.size() > 3)
	{
		colors0 = glm::vec4(colors[colors_size - 4], colors[colors_size - 3], colors[colors_size - 2], colors[colors_size - 1]);
	}

	glm::vec4 os_p0 = glm::vec4(vertices[size - 3], vertices[size - 2], vertices[size - 1], 1.0f);
	glm::vec3 ws_p0 = static_cast<glm::vec3>(transform * os_p0);

	for (uint32_t i = 0, j = 0; i + 2 < vertices.size(); i += 3, j += 4)
	{
		//add current vertex to the polygon
		bool adding_colors = colors_size >= j + 3;

		glm::vec4 colors1;

		AddVertexToPolygon(results[active_poly], static_cast<glm::vec3>(os_p0));

		if (adding_colors)
		{
			AddColorToPolygon(results[active_poly], colors0);
			colors1 = glm::vec4(colors[j], colors[j + 1], colors[j + 2], colors[j + 3]);
		}
		//get next vertex
		glm::vec4 os_p1 = glm::vec4(vertices[i], vertices[i + 1], vertices[i + 2], 1.0f);

		glm::vec3 ws_p1 = static_cast<glm::vec3>(transform * os_p1);
		glm::vec3 ws_p0_5;

		//check if the line segment between them is crossing the plane
		if (FindPlaneLineIntersection(plane, ws_p0, ws_p1, &ws_p0_5) == 2)
		{
			glm::vec3 os_p0_5 = static_cast<glm::vec3>(inv_transform * glm::vec4(ws_p0_5, 1.0f));
			glm::vec4 colors05;
			if (adding_colors)
			{
				colors05 = InterpolateColor(ws_p0_5, ws_p0, ws_p1, colors0, colors1);
				AddColorToPolygon(results[active_poly], colors05);
			}
			//if it is add the crossing point to both resulting polygons
			//and change to which polygon we're adding next vertices
			AddVertexToPolygon(results[active_poly], os_p0_5);

			active_poly = (active_poly + 1) % 2;

			AddVertexToPolygon(results[active_poly], os_p0_5);
			if (adding_colors)
			{
				AddColorToPolygon(results[active_poly], colors05);
			}
		}
		//change current vertex to next
		colors0 = colors1;
		os_p0 = os_p1;
		ws_p0 = ws_p1;
	}
}

inline glm::vec4 Tree::InterpolateColor(const glm::vec3& interpolationPoint, const glm::vec3& beginPointPos, const glm::vec3& endPointPos, const glm::vec4& beginPointColor, const glm::vec4& endPointColor)
{
	glm::vec4 resultColor;
	float ratio = static_cast<float>((interpolationPoint - beginPointPos).length() / (endPointPos - beginPointPos).length());
	resultColor = beginPointColor;
	resultColor += ratio * (endPointColor - beginPointColor);
	return resultColor;
}

inline void Tree::AddColorToPolygon(std::shared_ptr<Polygon> polygon, glm::vec4 & colors0)
{
	polygon->m_Colors.push_back(colors0.r);
	polygon->m_Colors.push_back(colors0.g);
	polygon->m_Colors.push_back(colors0.b);
	polygon->m_Colors.push_back(colors0.a);
}

void Tree::AddVertexToPolygon(std::shared_ptr<Polygon> polygon, glm::vec3& os_p0)
{
	polygon->m_Vertices.push_back(os_p0.x);
	polygon->m_Vertices.push_back(os_p0.y);
	polygon->m_Vertices.push_back(os_p0.z);
}

Node::Node()
{
}

Node::Node(std::shared_ptr<Polygon> polygon)
{
	polygons.push_back(polygon);
	plane = Plane(*polygon);
}


Node::~Node()
{
	if (front == nullptr)
		delete front;
	if (back == nullptr)
		delete back;
}

void AddSplitPolygonsToObject(Object* object, std::shared_ptr<Polygon> currentPolygon, std::shared_ptr<Polygon> p1, std::shared_ptr<Polygon> p2)
{
	currentPolygon = p1;
	object->m_Polygons.push_back(p2);
}
