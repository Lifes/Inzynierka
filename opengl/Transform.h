#pragma once
#include "glm.hpp"
#include "gtc/quaternion.hpp"

class Transform
{
public:
	Transform();
	~Transform();

	const glm::quat& GetRotation() const { return m_Rotation; }
	void SetRotation(const glm::quat& rotation);
	void Rotate(const glm::quat& rotation);

	virtual const glm::vec3& GetPosition() const { return m_Position; }
	virtual void SetPosition(const glm::vec3& position);
	virtual void Translate(const glm::vec3& translation);

	const glm::vec3& GetScale() const { return m_Scale; }
	void SetScale(const glm::vec3& scale);

	void UpdateTransformMatrix() const;

	const glm::mat4& GetTransformMatrix() const;
	glm::vec3 GetAxisZ() const;
	glm::vec3 GetAxisY() const;
	glm::vec3 GetAxisX() const;

protected:
	virtual void SetMatrix(const glm::mat4& position, const glm::mat4& rotation, const glm::mat4& scale) const;
	mutable glm::mat4 m_Transform;

private:
	glm::vec3 m_Position;
	glm::quat m_Rotation;
	glm::vec3 m_Scale = { 1.f, 1.f, 1.f };

	mutable bool m_IsDirty = true;
};

class CameraTransform : public Transform
{
protected:
	void SetMatrix(const glm::mat4& position, const glm::mat4& rotation, const glm::mat4& scale) const override;
};