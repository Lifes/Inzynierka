#include "Camera.h"
#include "Input.h"
#include "SDL.h"
#include "glm.hpp"
#include "gtc\matrix_transform.hpp"
#include "gtc\quaternion.hpp"
#include "gtx\quaternion.hpp"

Camera::Camera(float fov, float near, float far)
	: m_FOV(fov)
	, m_Near(near)
	, m_Far(far)
{
	SetProjectionMatrix();
	SetViewMatrix();
}

Camera::Camera(float width, float heigth, float fov, float near, float far)
	: m_ResWitdh(width)
	, m_ResHeight(heigth)
	, m_FOV(fov)
	, m_Near(near)
	, m_Far(far)
{
	SetProjectionMatrix();
	SetViewMatrix();
}

void Camera::Update(float dt)
{
	const Uint8* keys_pressed = SDL_GetKeyboardState(NULL);
	glm::vec3 movement_vector;

	if (keys_pressed[SDL_SCANCODE_W])
	{
		if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
		{
			movement_vector.z += 1;
		}
		else
		{
			movement_vector.y -= 1;
		}
	}
	if (keys_pressed[SDL_SCANCODE_S])
	{
		if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
		{
			movement_vector.z -= 1;
		}
		else
		{
			movement_vector.y += 1;
		}
	}
	if (keys_pressed[SDL_SCANCODE_A])
	{
		movement_vector.x += 1;
	}
	if (keys_pressed[SDL_SCANCODE_D])
	{
		movement_vector.x -= 1;
	}

	if (movement_vector.length() > 0)
		glm::normalize(movement_vector);

	movement_vector *= m_MovementVelocity * dt;

	m_Transform.Translate(movement_vector * m_Transform.GetRotation());

	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(SDL_BUTTON_RIGHT))
	{
		int dx, dy;
		Input::Instance().GetMouseRelativeState(&dy, &dx);
		glm::quat qx = glm::angleAxis(dx * m_AngularVelocity, m_Transform.GetAxisX());
		glm::quat qy = glm::angleAxis(dy * m_AngularVelocity, m_Transform.GetAxisY());
		glm::quat rotation = glm::normalize((qx * qy));
		m_Transform.Rotate(rotation);
	}

	UpdateFrustum();

}

void Camera::SetFov(float fov)
{
	m_FOV = fov;
	SetProjectionMatrix();
}

void Camera::SetNearPlane(float near)
{
	m_Near = near;
	SetProjectionMatrix();
}

void Camera::SetFarPlane(float far)
{
	m_Far = far;
	SetProjectionMatrix();
}

void Camera::SetResolution(float width, float height)
{
	m_ResHeight = height;
	m_ResWitdh = width;
	SetProjectionMatrix();
}

glm::vec3 Camera::GetPosition()
{
	glm::vec3 result = m_Transform.GetPosition();
	return -result;
}


void Camera::SetProjectionMatrix()
{
	m_Projection = glm::perspective(m_FOV, m_ResWitdh / m_ResHeight, m_Near, m_Far);
}

void Camera::SetViewMatrix()
{
	//starting camera options, use rotate/translate to move camera
	m_Transform.SetPosition(glm::vec3(0, 0, -4));
	m_Transform.SetRotation(glm::quat());
}

void Camera::UpdateFrustum()
{
	auto vp = GetViewProjectionMatrix();
	m_Frustum[0].n.x = vp[0][3] - vp[0][0];
	m_Frustum[0].n.y = vp[1][3] - vp[1][0];
	m_Frustum[0].n.z = vp[2][3] - vp[2][0];
	m_Frustum[0].d = vp[3][3] - vp[3][0];

	m_Frustum[1].n.x = vp[0][3] + vp[0][0];
	m_Frustum[1].n.y = vp[1][3] + vp[1][0];
	m_Frustum[1].n.z = vp[2][3] + vp[2][0];
	m_Frustum[1].d = vp[3][3] + vp[3][0];

	m_Frustum[2].n.x = vp[0][3] - vp[0][1];
	m_Frustum[2].n.y = vp[1][3] - vp[1][1];
	m_Frustum[2].n.z = vp[2][3] - vp[2][1];
	m_Frustum[2].d = vp[3][3] - vp[3][1];

	m_Frustum[3].n.x = vp[0][3] + vp[0][1];
	m_Frustum[3].n.y = vp[1][3] + vp[1][1];
	m_Frustum[3].n.z = vp[2][3] + vp[2][1];
	m_Frustum[3].d = vp[3][3] + vp[3][1];

	m_Frustum[4].n.x = vp[0][3] - vp[0][2];
	m_Frustum[4].n.y = vp[1][3] - vp[1][2];
	m_Frustum[4].n.z = vp[2][3] - vp[2][2];
	m_Frustum[4].d = vp[3][3] - vp[3][2];

	m_Frustum[5].n.x = vp[0][3] + vp[0][2];
	m_Frustum[5].n.y = vp[1][3] + vp[1][2];
	m_Frustum[5].n.z = vp[2][3] + vp[2][2];
	m_Frustum[5].d = vp[3][3] + vp[3][2];

	//normalize
	for (auto f : m_Frustum)
	{
		float t = 1.f / f.n.length();
		f.n *= t;
		f.d *= t;
	}
}
