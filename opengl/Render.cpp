#include "Render.h"
#include "Color.h"
#include "Scene.h"
#include "Object.h"
#include "Shader.h"
#include "Float.h"
#include "PVSTree.h"

#include "Libs/glew/glew.h"
#include "glm.hpp"
#include "gtc\matrix_transform.hpp"

#include <memory>
#include <SDL.h>

bool Renderer::m_RenderWireframe = false;
bool Renderer::m_EnableBackfaceCulling = false;
bool Renderer::m_EnableDepthBuffer = false;

void ClearColor(const Color& color, SDL_Window* mainWindow)
{
	glClearColor(color.m_Red, color.m_Green, color.m_Blue, color.m_Alpha);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(mainWindow);
}

Renderer::Renderer(const Color& backgroundColor, SDL_Window* mainWindow, Camera* camera, const std::string& vertexShaderPath, const std::string& fragmentShaderPath)
	: m_BackgroundColor(backgroundColor)
	, m_MainWindow(mainWindow)
	, m_Camera(camera)
{
	m_Shader = new Shader(vertexShaderPath, fragmentShaderPath);
	m_Shader->Run();
}

Renderer::~Renderer()
{
	if (m_Shader != nullptr)
		delete m_Shader;
}

void Renderer::StartFrame()
{
	glClearColor(m_BackgroundColor.m_Red, m_BackgroundColor.m_Green, m_BackgroundColor.m_Blue, m_BackgroundColor.m_Alpha);
	if(m_EnableDepthBuffer)
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	else
		glClear(GL_COLOR_BUFFER_BIT);
}

void Renderer::EndFrame()
{
	SDL_GL_SwapWindow(m_MainWindow);
}

void Renderer::RenderFrame(Scene* scene)
{
	StartFrame();
	RenderScene(scene);
	EndFrame();
}

void Renderer::RenderScene(Scene* scene)
{
	m_PolyCount = 0;
	scene->UpdatePolygonList();
	//if depth buffer not enabled we render using BSP tree for polygon ordering
	if(!m_EnableDepthBuffer)
		RenderTree(scene->GetSceneTree());
	else	//else we can use PVS to reduce the amount of polygons we have to render
		RenderTree(scene->GetPvsSceneTree());
	
}

void Renderer::RenderPolygon(std::shared_ptr<Polygon> polygon)
{
	++m_PolyCount;
	//Get mvp matrix for current polygon and send it to shader
	auto mvp = m_Camera->GetViewProjectionMatrix() * polygon->GetParent()->GetTransform();
	m_Shader->SetMVPMatrix(mvp);
	//render polygon
	polygon->Render(m_RenderWireframe);
}

void Renderer::RenderTree(std::shared_ptr<Tree> tree)
{
	RenderTreeNode(tree->m_Root, m_Camera->GetPosition());
}

void Renderer::RenderTree(std::shared_ptr<PVSTree> tree)
{
	uint32_t node_id = 0;
	while (true)
	{
		auto camera_pos = m_Camera->GetPosition();
		const PVSNode& current_node = tree->m_Nodes[node_id];
		const Plane& plane = tree->m_Planes[current_node.plane];
		auto dot = glm::dot(plane.n, camera_pos - plane.p0);
		if (dot >= 0.f)
		{
			if (current_node.is_leaf)
			{
				RenderPVSTree(tree, current_node.front);
				return;
			}
			else
			{
				node_id = current_node.front;
			}
		}
		else
		{
			if (current_node.back == -1) //we're inside a wall/object
			{
				return;
			}
			else
			{
				node_id = current_node.back;
			}
		}
	}
}

void Renderer::RenderTreeNode(Node* node, const glm::vec3& viewerPos)
{
	auto parent = node->polygons[0]->GetParent();
	auto dot = glm::dot(node->plane.n, viewerPos - node->plane.p0);
	if (node->front == nullptr && node->back == nullptr) //node is a leaf
	{
		if (m_EnableBackfaceCulling && dot < 0.f)
			return;
		for(const auto& poly : node->polygons)
			RenderPolygon(poly);
		return;
	}

	if (Cmpf(dot, 0.f)) //viewer exactly on plane
	{
		if (node->front != nullptr)
			RenderTreeNode(node->front, viewerPos);
		if (node->back != nullptr)
			RenderTreeNode(node->back, viewerPos);
	}
	else if (dot > 0.f) //viewer in front
	{
		if (node->back != nullptr)
			RenderTreeNode(node->back, viewerPos);

		for (const auto& poly : node->polygons)
			RenderPolygon(poly);

		if (node->front != nullptr)
			RenderTreeNode(node->front, viewerPos);
	}
	else //viewer behind
	{
		if (node->front != nullptr)
			RenderTreeNode(node->front, viewerPos);

		if(!m_EnableBackfaceCulling)
			for (const auto& poly : node->polygons)
				RenderPolygon(poly);

		if (node->back != nullptr)
			RenderTreeNode(node->back, viewerPos);
	}
}

void Renderer::RenderPVSTree(std::shared_ptr<PVSTree> tree, uint32_t leafId)
{
	for (auto leaf_id : tree->m_PvsData[leafId])
	{
		for (uint32_t i = tree->m_Leafs[leaf_id].start_polygon; i < tree->m_Leafs[leaf_id].end_polygon; ++i)
		{
			if (m_EnableBackfaceCulling)
			{
				Plane plane(*tree->m_Polygons[i]);
				if(glm::dot(plane.n, m_Camera->GetPosition() - plane.p0) < 0.f)
					continue;
			}
			RenderPolygon(tree->m_Polygons[i]);
		}
	}
}

bool Renderer::IsPVSLeafInCameraFrustum(std::shared_ptr<PVSTree> tree, uint32_t leafId)
{
	const auto& frustum = m_Camera->GetFrustumArray();

	glm::vec3 box_min = tree->m_Leafs[leafId].bounding_box.min;
	glm::vec3 box_max = tree->m_Leafs[leafId].bounding_box.max;
	glm::vec3 near(0.f);

	for (const auto& f : frustum)
	{
		if (f.n.x > 0.f)
			near.x = box_min.x;
		else
			near.x = box_max.x;

		if (f.n.y > 0.f)
			near.y = box_min.y;
		else 
			near.y = box_max.y;

		if(f.n.z > 0.f)
			near.z = box_min.z;
		else
			near.z = box_max.z;

		if (glm::dot(near, f.n) > f.d)
			return false;
	}
	return true;
}


