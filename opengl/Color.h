#pragma once

class Color
{
public:
	static Color RED();
	static Color GREEN();
	static Color BLUE();
	static Color BLACK();
	static Color WHITE();

	Color();
	Color(float red, float green, float blue, float alpha = 1.f);
	~Color();


	float m_Red;
	float m_Green;
	float m_Blue;
	float m_Alpha;
};