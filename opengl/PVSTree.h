#pragma once
#include "Tree.h"
#include "glm.hpp"
#include "Object.h"

#include <limits>
#include <array>

struct BBox
{
	glm::vec3 min = { std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max(), std::numeric_limits<int32_t>::max() };
	glm::vec3 max = { std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::min(), std::numeric_limits<int32_t>::min() };
};


struct Leaf
{
	uint32_t start_polygon; //index of first polygon belonging to this leaf
	uint32_t end_polygon; //index of first polygon of next leaf
	BBox	bounding_box;
	std::vector<uint32_t> portals_indices;
};

struct PVSNode
{
	bool	is_leaf = false;
	uint32_t front; //index of front node, if is_leaf then index of leaf
	int32_t back;	//index of back node
	uint32_t plane;
	BBox	bounding_box;
};


struct Portal
{
	uint32_t id;
	Polygon portal;
	uint8_t leafs_amount = 0;
	std::array<uint32_t,2> leafs;
};

//http://www.cs.utah.edu/~jsnider/SeniorProj/BSP/default.htm
class PVSTree : public Tree
{
	friend class Renderer;
public:
	PVSTree(polygon_list polygons);

	void MakeTree(uint32_t nodeId, polygon_list& polygons);
private:
	std::vector<Leaf>		m_Leafs;
	std::vector<PVSNode>	m_Nodes;
	std::vector<Plane>		m_Planes;
	polygon_list			m_Polygons;
	std::vector < std::shared_ptr<Portal>> m_Portals;
	std::vector<std::vector<uint32_t>> m_PvsData;
	
	void BuildPortals();

	void CalculatePVS();
	void CalculateLeafsPVS(uint32_t leafId, std::shared_ptr<Portal> sourcePortal, std::shared_ptr<Portal> destPortal, 
		uint32_t destLeafId, std::vector<uint32_t>& pvs, std::vector<uint32_t>& visitedThrough);
	bool ClipWithAntiPenumbra(std::shared_ptr<Portal> source, std::shared_ptr<Portal> destination, std::shared_ptr<Portal> generator);
	void CalculateBBox(BBox* box, polygon_list& polygons);
	void CalculateBBox(BBox* box, std::shared_ptr<Polygon> polygon);
	std::vector<std::shared_ptr<Portal>> ClipPortal(std::shared_ptr<Portal> portal, uint32_t node_id);
	std::vector<std::shared_ptr<Portal>> ClipSpanningPortal(std::shared_ptr<Portal> portal, uint32_t node_id);
	std::vector<std::shared_ptr<Portal>> ClipOnBackPortal(uint32_t node_id, std::shared_ptr<Portal> portal);
	std::vector<std::shared_ptr<Portal>> ClipInFrontPortal(uint32_t node_id, std::shared_ptr<Portal> portal);
	std::vector<std::shared_ptr<Portal>> ClipOnPlanePortal(uint32_t node_id, std::shared_ptr<Portal> portal);
	bool IsPortalDuplicate(std::shared_ptr<Portal> portal, int32_t* index);
	std::shared_ptr<Portal> CalculatePortal(uint32_t node_id);
	std::shared_ptr<Portal> CalculatePortal(const PVSNode& node);
	uint32_t GetBestSplitter(polygon_list& polygons, polygon_list& frontPolygons);
};

