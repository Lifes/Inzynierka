#pragma once

struct Point
{
	Point() : X(0.f), Y(0.f), Z(0.f) {};
	Point(int x, int y, int z) : X(x), Y(y), Z(z){};

	float X, Y, Z;
};