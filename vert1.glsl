#version 330

in vec3 in_Position;
in vec4 in_Color;

uniform mat4 mvp;

out vec4 ex_Color;

void main(void) {
  
	vec4 position = vec4(in_Position, 1.0f);
	gl_Position = mvp * position;

    ex_Color = in_Color;
}
